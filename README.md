# Investree Front-end

to run the app and see logic result, first you need to clone this repository



**to run simple logic test, go to folder /simpleLogic, and run logic.js using node/yarn  etc , for example if you are using node**

```bash
node logic.js
```



**to run simpleapp test, go to folder /simpleapp, and** 

1. install all dependencies, 

   ```bash
   npm install
   ```

2. run using command

   ```bash
   npm run serve
   ```



note : this simple application developed considering only 1366 x 768 and below resolution, and it work proper enough on that resolution.

