//run by typing node logic.js in terminal

const isPalindrome = (str) => {
    let reverseStr = str.split('').reverse().join('')
    if(reverseStr === str){
        return console.log(`isPalindrome(${str})`,true)
    }else{
        return console.log(false)
    }
}

const findPrimeByRange = (a, b) => {
    const cekPrime = (num) => {
        let flag = true
        for (var i = 2; i < num; i++){ 
            if (num % i === 0)flag = false
        }
        return flag;
    }
    let output = []
    for(let i = a; i <= b; i++){
        if(cekPrime(i))output.push(i)
    }
    return console.log(`findPrimeByRange(${a},${b})`,output)
}

const group = arr => {
    const inputSorted = arr.sort()
    let output = []
    let temporaryContainer = []
    for(let i = 0; i < inputSorted.length; i++){
        const letter = inputSorted[i]
        const prevLetter = inputSorted[i-1]
        if(i !== 0 && letter !== prevLetter){
            output.push(temporaryContainer)
            temporaryContainer = []
            temporaryContainer.push(letter)
        }else{
            temporaryContainer.push(letter)
        }
    }
    output.push(temporaryContainer)
    return console.log(`group[${arr}]`,output)
}


const arrToObj = arr => {
    let output = {}
    arr.forEach(letter => {
        if(!output[letter])output[letter] = 1
        else output[letter] ++
    });
    return console.log(`arrToObj(${arr})`,output)
}

isPalindrome('abcba')
findPrimeByRange(11,40)
group(['a', 'a', 'a', 'b', 'c', 'c', 'b', 'b', 'b', 'd', 'd', 'e', 'e', 'e'])
arrToObj(['a', 'a', 'a', 'b', 'c', 'c', 'b', 'b', 'b', 'd', 'd', 'e', 'e', 'e'])

